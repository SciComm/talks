# GitLab repository for talk proposals in PhyComm

To propose a talk, please open an issue. To do that, you'll first have to create a GitLab account and then navigate to this repository and on the sidebar click "Issues" and click the "New Issue" button. You can fill in your talk's details there. Thanks for your contribution!
