# Abstract

A gist of what you would like to be covered in the talk/workshop.

# Skill level

To what skill level should the talk/workshop be geared towards?

# References

Any references you would like to share that you think should be covered in the talk/workshop?

# Misc.

Anything else you'd like to add such as specific questions you'd like answered or notes about how you'd like the talk/workshop conducted?
